import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <header className="w-100  bg-red-light">
        <div className="container  text-center">
          <img src="https://www.bullring.co.uk/Data/skins/vicinitee-bullring-web-10years/images/logo.png?v=1" alt="Birmingham Bull Ring" className="my-6" />
        </div>
      </header>
    );
  }
}

export default Header;
