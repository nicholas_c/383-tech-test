import React, { Component } from 'react';
import Autolinker from 'autolinker';
import TimeAgo from 'react-timeago'

import TwitterIcon from './../img/twitter.svg';
import InstagramIcon from './../img/instagram.svg';

class TwitterItem extends Component {
  render() {
    const itemData = this.props.data;
    let itemText = '';
    let itemIcon = {};
    let itemImage = false;
    let autolinker = new Autolinker({
      className: 'text-red  no-underline'
    });

    switch (itemData.service_name) {
      case 'Twitter':
        autolinker = new Autolinker({
          mention: 'twitter',
          hashtag: 'twitter',
          className: 'text-red  no-underline'
        });
        itemText = autolinker.link(itemData.item_data.tweet);
        itemIcon = {
          type: 'icon',
          content: TwitterIcon,
        };
        break;
      case 'Instagram':
        autolinker = new Autolinker({
          mention: 'instagram',
          hashtag: 'instagram',
          className: 'text-red  no-underline'
        });
        itemImage = itemData.item_data.image.medium;
        itemText = autolinker.link(itemData.item_data.caption);
        itemIcon = {
          type: 'icon',
          content: InstagramIcon,
        };
        break;
      case 'Manual':
      default:
        itemImage = itemData.item_data.image_url;
        itemText = autolinker.link(itemData.item_data.text);
        itemIcon = {
          type: 'text',
          content: 'AFF',
        };
        break;
    }

    return (
      <section className="p-2  feed-item">
        <div className="p-4  pt-12  border  border-grey-light  border-1  rounded  relative">
          <div className="feed-item__type  absolute">
            { itemIcon.type === 'icon' && <img className="rounded  bg-black  p-2  pt-4  h-12  w-10" src={itemIcon.content} alt={itemData.service_name} />}
            { itemIcon.type !== 'icon' && <p className="rounded  bg-red  p-2  pt-6  text-white">{ itemIcon.content }</p> }
          </div>

          { itemImage && <img src={`https://picsum.photos/300/200?${itemData.item_id}`} alt={itemData.item_data.link_text} className="feed-item__img  w-100  pb-4" /> }

          <p className="text-sm  text-grey-darker  leading-tight  mb-4" dangerouslySetInnerHTML={{ __html: itemText }} />

          <p className="text-sm  leading-tight  text-grey">Posted <TimeAgo date={itemData.item_published} /></p>
        </div>
      </section>
    );
  }
}

export default TwitterItem;
