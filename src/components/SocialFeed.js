import React, { Component } from 'react';
import Feed from './Feed.js'

class SocialFeed extends Component {
  render() {
    return (
      <section>
        <div className="text-center  my-4">
          <h1 className="text-4xl  text-red">
            Autumn Fashion Fix
          </h1>

          <h2 className="text-xl">
            #AFF
          </h2>
        </div>

        <Feed />
      </section>
    );
  }
}

export default SocialFeed;
