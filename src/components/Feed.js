import React, { Component } from 'react';
import Axios from 'axios';
import FeedItem from './FeedItem';

class Feed extends Component {
  constructor(props) {
    super(props);
    this.page = 1;

    this.state = {
      feedData: [],
      filteredData: [],
      isLoading: false,
      filters: {
        sort: 'date-desc',
        service: 'all'
      }
    }
  }

  fetchData(promise) {
    Axios.get(`http://private-cc77e-aff.apiary-mock.com/posts?page=${this.page++}`)
      .then(data => {
        if (data.status === 200) {
          data.data.items.forEach(el => {
            el.item_id += `-${this.page}`
            this.state.feedData.push(el)

            if (this.state.filters.service === 'all') {
              this.state.filteredData.push(el)
            }
          })

          this.setState({
            isLoading: false
          });

          // this.changeFilter('service', this.state.filters['service'])
        }
      })
  }

  loadMore() {
    this.fetchData()
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    this.fetchData()
  }

  changeFilter(type, filter) {
    if (type === 'sort') {
      this.state.filters.sort = type
      this.state.filteredData.sort((a, b) => {
        if (filter.split('-')[1] === 'asc') {
          return a.item_published > b.item_published;
        } else {
          return a.item_published < b.item_published;
        }
      });
    }

    if (type === 'service' && filter === 'all') {
      this.state.filters.service = 'all'
      this.state.filteredData = this.state.feedData
    }

    if (type === 'service' && filter !== 'all') {
      this.state.filters.service = filter
      this.state.filteredData = []

      this.state.feedData.forEach(el => {
        if (el.service_name.toLowerCase() === filter) {
          this.state.filteredData.push(el)
        }
      })
    }

    this.setState({
      filteredData: this.state.filteredData
    });
  }

  render() {
    const { filteredData, isLoading } = this.state;

    if (isLoading) {
      return (
        <div className="loader">
          <div className="loader__inner">
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className="bg-white  flex  justify-center  px-2  py-4  sticky  pin-t  z-20">
          <div className="w-1/2  md:w-1/5  p-2  relative  filter">
            <p>Sort by <svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"/></svg></p>

            <ul className="absolute  pin-x  bg-white  z-10  filter__items  list-reset">
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('sort', 'date-desc') } }>New to old</li>
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('sort', 'date-asc') } }>Old to new</li>
            </ul>
          </div>

          <div className="w-1/2  md:w-1/5  p-2  relative  filter">
            <p>Filter service <svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"/></svg></p>

            <ul className="absolute  pin-x  bg-white  z-10  filter__items  list-reset">
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('service', 'all') } }>All</li>
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('service', 'instagram') } }>Instagram</li>
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('service', 'twitter') } }>Twitter</li>
              <li className="hover:bg-red  hover:text-white  px-2  py-2" onClick={ () => { this.changeFilter('service', 'manual') } }>Birmingham Bull Ring</li>
            </ul>
          </div>
        </div>

        <div className="feed">
          {filteredData.map((el, i) => {
            return <FeedItem key={el.item_id} data={el} />
          })}
        </div>

        <div className="feed__more  flex  justify-center  pb-24  pt-12">
          <div className="bg-red  rounded-lg  w-64  text-white  text-center  p-4  cursor-pointer" onClick={() => { this.loadMore() } }>
            Load more
          </div>
        </div>
      </div>
    );
  }
}

export default Feed;
