import React, { Component } from 'react';
import Header from './components/Header.js'
import SocialFeed from './components/SocialFeed.js'
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header />

        <main className="container">
          <SocialFeed />
        </main>
      </div>
    );
  }
}

export default App;
